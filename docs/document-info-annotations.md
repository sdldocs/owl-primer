# 문서 정보와 주석(Document Information and Annotations)
다음에서 온톨로지에 명시된 "논리적" 지식에 실제로 기여하지 않는 OWL 2의 기능을 설명한다. 오히려 이들은 온톨로지 자체, 공리 또는 단일 엔터티에 대한 추가 정보를 제공하는 데 사용된다.

## 공리와 엔티티에 주석 달기(Annotating Axioms and Entities)
많은 경우에 OWL 온톨로지의 일부에 실제로 domain 자체를 기술하지 않고 도메인 기술에 대한 정보를 제공하기도 한다. OWL은 이를 위해 주석을 제공한다. OWL 주석은 단순히 속성-값 쌍을 온톨로지의 일부 또는 전체 온톨로지 자체와 연관시킨다. 주석 자체에도 주석을 달 수 있다. 주석 정보는 실제로 온톨로지의 논리적 의미의 일부가 아니다.

예를 들어 온톨로지의 클래스 중 하나에 정보를 추가하여 의미에 대한 자연어 설명을 제공할 수 있다.

**함수 스타일 syntax**
```
AnnotationAssertion( rdfs:comment :Person "Represents the set of all people." )
```

다음은 공리에 대한 주석의 예이다.

**함수 스타일 syntax**
```
SubClassOf( 
   Annotation( rdfs:comment "States that every man is a person." )
   :Man 
   :Person 
)
```

종종 도움말 인터페이스에 표시될 자연어 텍스트에 대한 액세스를 제공하는 도구가 이러한 주석을 사용한다.

## 온톨로지 관리(Ontology Management) <a id="ontology-management"></a>
OWL에서 주제에 대한 일반 정보를 거의 항상 온톨로지로 수집되어 다양한 어플리케이션에서 사용한다. 또한 일반적으로 웹에서 온톨로지 문서가 있는 위치인 OWL 온톨로지의 이름을 제공할 수 있다. 다른 어플리케이션에서 사용되는 경우 주제에 대한 특정 정보를 온톨로지에 서술할 수도 있다.

**함수 스타일 syntax**
```
Ontology(<http://example.com/owl/families>
   ...
)
```

OWL 온톨로지를 OWL 문서에 작성한 다음 로컬 파일 시스템이나 World Wide Web에 저장한다. OWL 온톨로지를 포함하는 것 외에도 OWL 문서에는 접두사에 대한 확장을 제공하여 OWL 온톨로지에서 일반적으로 사용되는 약어(예: Person)를 IRI로 변환하는 정보도 포함할 수 있다. IRI는 접두어 확장과 참조의 연결이다.

이 예에서 지금까지 `xsd`와 빈 접두사를 포함하여 많은 접두사를 사용했다. `xsd` 접두사는 XML 스키마 권장 사항으로 IRI가 수정된 XML 스키마 데이터 타입의 약어로 사용하였다. 따라서 `http://www.w3.org/2001/XMLSchema#`인 `xsd`를 표준 확장으로 사용해야 한다. 다른 접두사에 대해 선택한 확장은 온톨로지 자체의 이름뿐만 아니라 온톨로지의 클래스, 속성과 개체의 이름에도 영향을 미친다. 웹에 온톨로지를 배치하려면 제어하는 ​​웹의 일부에 있는 확장을 선택하여 다른 사람의 이름을 우연히 사용하지 않도록 해야 한다 (여기서는 아무도 제어하지 않는 구성된 장소를 사용한다). 두 가지 XML 기반 구문에는 기본적으로 제공되는 이름에 대한 내임스페이스가 필요하며 내임스페이스에 대해 XML 엔티티 참조를 사용할 수도 있다. 일반적으로 유사한 키워드가 사용되는 경우에도 사용 가능한 약어 메커니즘과 특정 구문이 OWL의 직렬화마다 다르다는 점에 유의해야 한다.

**함수 스타일 syntax**
```
Prefix(:=<http://example.com/owl/families/>)
Prefix(otherOnt:=<http://example.org/otherOntologies/families/>)
Prefix(xsd:=<http://www.w3.org/2001/XMLSchema#>)
Prefix(owl:=<http://www.w3.org/2002/07/owl#>)
 
Ontology(<http://example.com/owl/families>
   ...
 )
```

또한 OWL에서는 한 온톨로지에 저장된 일반 정보를 다른 온톨로지에 재사용하는 것이 일반적이다. 이 정보의 복사를 요구하는 대신 OWL은 다음과 같이 import 문을 사용하여 다른 온톨로지의 전체 온톨로지 내용을 가져올 수 있다.

**함수 스타일 syntax**
```
Import( <http://example.org/otherOntologies/families.owl> )
```

시맨틱 웹과 온톨로지를 분산 환경으로 구축하면 온톨로지는 동일한 개념, 속성과 개체에 대해 서로 다른 이름을 사용하는 것이 일반적이다. 이미 보았듯이 OWL의 여러 구조는 다른 이름이 동일한 클래스, 속성 또는 개체를 참조한다는 것을 나타내는 데 사용할 수 있다. 예를 들어, 엔티티 이름을 바꾸는 대신 가져온 온톨로지에 사용된 이름을 다음과 같이 온톨로지에 사용되는 이름으로 묶을 수 있다.

**함수 스타일 syntax**
```
SameIndividual( :John otherOnt:JohnBrown )
SameIndividual( :Mary otherOnt:MaryBrown )
EquivalentClasses( :Adult otherOnt:Grownup )
EquivalentObjectProperties( :hasChild otherOnt:child )
EquivalentDataProperties( :hasAge otherOnt:age )
```

## 엔티티 선언(Entity Declarations)
온톨로지 관리를 돕기 위해 OWL에는 선언이라는 개념이 있다. 기본 개념은 각 클래스, 속성 또는 개체를 온톨로지에서 선언하여야 하며, 그런 다음 해당 온톨로지와 해당 온톨로지를 가져오는 온톨로지에서 사용할 수 있다는 것이다.

`Manchester` 구문에서 선언은 암묵적이다. 클래스, 속성 또는 개체에 대한 정보를 제공하는 구문은 필요한 경우 해당 클래스, 속성 또는 개체를 암묵적으로 선언한다. 다른 구문에서는 명시적으로 선언해야 한다.

**함수 스타일 syntax**
```
Declaration( NamedIndividual( :John ) )
Declaration( Class( :Person ) )
Declaration( ObjectProperty( :hasWife ) )
Declaration( DataProperty( :hasAge ) )
```

그러나 IRI는 동시에 다른 엔티티 타입(예: 개체와 클래스 모두)을 나타낼 수 있다. "말장난(punning)"이라 하는 이 가능성은 어느 정도의 메타모델링을 허용하기 위해 도입되었다. [OWL 2 DL과 OWL 2 Full](owl2dl-full.md)에서 이에 대한 예를 보인다. 그래도 OWL 2는 이름을 사용하고 재사용하는 데 약간의 훈련이 필요하다. 더 읽기 쉬운 구문을 허용하고 기타 기술적인 이유로 OWL 2 DL은 이름이 한 속성 타입(객체, 데이터 타입 또는 주석 속성)에 대해서만 사용되어야 하며 한 IRI는 클래스와 데이터 타입 모두를 나타낼 수 없다. 더우기 "내장된" 이름(예: RDF, RDFS와 OWL의 다양한 구문에서 사용되는 이름)은 OWL에서 자유롭게 사용할 수 없다.
