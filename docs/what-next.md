# 다음으로 읽을 거리(What To Read Next)
이 짧은 프라이머는 OWL의 표면만 볼 수 있다. OWL과 웹에서 검색하여 찾을 수 있는 OWL 도구 사용 방법에 대한 더 길고 자세한 문서가 많이 있다. 이러한 문서 중 하나를 읽고 도구를 사용하여 OWL 온톨로지를 구축하는 것이 OWL에 대한 실무 지식을 얻는 가장 좋은 방법일 것이다. OWL의 기초에 대해 자세히 알아보려면 먼저 교과서 ([FOST](https://www.w3.org/TR/2012/REC-owl2-primer-20121211/#ref-FOST))를 참조한 다음 그 책에 인용된 원본 기사를 참조하는 것이 좋다. 웹에서도 볼 수 있는 [OWL 2 Profiles: An Introduction to Lightweight Ontology Languages](http://korrekt.org/page/OWL_2_Profiles)에서 OWL 2 Profiles에 대한 자세한 소개를 찾을 수 있다.

이 짧은 문서는 OWL의 규범적 정의가 아니다. OWL 구문의 규범적 정의와 각 OWL 구성의 의미에 대한 유익한 설명을 OWL 2 Structural Specification 및 Functional Syntax 문서 [OWL 2 Web Ontology Language: Structural Specification and Functional-Style Syntax (Second Edition)](http://www.w3.org/TR/owl2-syntax/)에서 찾을 수 있다.

OWL 2 Quick Reference Guide([OWL 2 Web Ontology Language: Quick Reference Guide (Second Edition)](http://www.w3.org/TR/owl2-quick-reference/))는 특정 언어 기능에 대한 정보를 찾을 때 참고하면 편리하다.

보다 형식적인 문서에 관심이 있는 사람들을 위해 OWL 2의 형식적 의미는 OWL 2 Semantics 문서([OWL 2 Web Ontology Language: Direct Semantics (Second Edition)](http://www.w3.org/TR/owl2-direct-semantics/),  [OWL 2 Web Ontology Language: RDF-Based Semantics (Second Edition)](http://www.w3.org/TR/owl2-rdf-based-semantics/))에서 찾을 수 있다.

OWL 구문과 RDF 트리플 간의 매핑은 OWL 2 Mapping to RDF Graphs 문서([OWL 2 Web Ontology Language: Mapping to RDF Graphs (Second Edition)](http://www.w3.org/TR/owl2-mapping-to-rdf/))에서 찾을 수 있다.
