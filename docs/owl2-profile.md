# OWL 2 프로파일(OWL 2 Profiles)
OWL 2 DL과 OWL 2 Full 외에도 OWL 2는 세 가지 프로필을 지정한다. 일반적으로 OWL 2는 매우 표현력이 풍부한 언어(계산적으로(computationally)와 사용자 모두)이므로 잘 구현하고 작업하기 어려울 수 있다. 이러한 추가 프로필은 다양한 어플리케이션에 충분히 접근 가능한 OWL 2 부분집합으로 설계되었다. OWL 2 DL과 마찬가지로 계산적 고려 사항은 이러한 프로필의 주요 요구 사항이지만 (그리고 기존 기술을 고려할 때 강력한 확장성으로 구현하기가 훨씬 더 쉽다), 우수한 계산 속성을 가진 OWL 2의 부분집합이 많이 있다. 선택된 OWL 2 프로필에는 이미 상당한 사용자 커뮤니티가 있지만 포함되지 않은 다른 프로필도 있고 더 많아질 것으로 예상된다. [OWL 2 Web Ontology Language: Profiles (Second Edition)]( http://www.w3.org/TR/owl2-profiles/.) 문서는 추가 프로필을 지정하기 위한 명확한 템플릿을 제공한다.

확장 가능한 추론을 보장하기 위해 기존 프로필은 표현력과 관련하여 몇 가지 제한 사항을 공유한다. 일반적으로 부정과 분리(disjunction)를 허용하지 않는다. 이러한 구성은 추론을 복잡하게 만들고 모델링에 거의 필요하지 않은 것으로 판명되었기 때문이다. 예를 들어, 어떤 프로필에서도 모든 사람이 남성 또는 여성임을 지정할 수 있다. 프로파일의 더 구체적인 모델링 제한 사항은 개별 프로파일에 대한 섹션에서 다룰 것이다.

각 프로필과 그것의 디자인 근거에 대해 논의하고 사용자가 작업할 프로필을 선택할 때 몇 가지 지침을 제공한다. 이 논의는 포괄적이지 않으며 그럴 수도 없다. 모든 결정의 일부는 사용 가능한 도구와 이것이 시스템 또는 워크플로우의 나머지 부분과의 적합도에 따라 결정되어야 한다. 프로파일에 대한 보다 자세한 논의와 비교는 [OWL 2 Profiles: An Introduction to Lightweight Ontology Languages](http://korrekt.org/page/OWL_2_Profiles) 문서에 있다.

대체로 서로 다른 프로필을 구문적으로 구분할 수 있으며 일부 프로필에는 다른 프로필이 포함되어 있다. 예를 들어, OWL 2 DL은 OWL 2 Full의 구문 일부분으로 볼 수 있으며 OWL 2 QL은 OWL 2 DL(따라서 OWL 2 Full) 구문의 부분이다. 아래 프로필 중 어느 것도 다른 프로필의 부분집합은 아니다. 이상적으로는 파생된 결과의 변경 없이 하위 프로필에서 상위 프로필을 준수하는 추론기(또는 다른 도구)를 사용할 수 있다. 특히, OWL 2 DL을 준수하는 모든 추론기는 OWL 2 EL, OWL 2 RL 및 OWL 2 QL을 준수하는 추론기이기도 하다 (그러나 OWL 2 DL 추론기가 보다 일반적인 경우 집합에 맞게 조정되기 때문에 성능이 다를 수 있다). OWL 2의 두 가지 의미 체계([OWL 2 DL과 OWL 2 Full](owl2dl-full.md) 참조) 각각은 모든 프로필에 사용할 수 있지만 OWL 2 EL과 OWL 2 QL에는 Direct Semantics를 사용하고 OWL 2 RL에는 RDF-Based Semantics를 사용하는 것이 가장 일반적이다.

## OWL 2 EL
OWL 2 EL로 작업하는 것은 OWL 2 DL로 작업하는 것과 상당히 유사하다. `subClassOf` 문의 양쪽에 클래스 표현을 사용할 수 있고 심지어 그러한 관계를 유추할 수도 있다. 많은 대규모 클래스 표현 지향(expression-oriented) 온톨로지의 경우 약간만 단순화하면 OWL 2 EL 온톨로지를 얻을 수 있고 원래 온톨로지의 의미 대부분을 보존할 수 있다.

OWL 2 EL은 SNOMED-CT, NCI 시소러스, Galen과 같은 대규모 바이오헬스 온톨로지를 염두에 두고 설계되었다. 이러한 온톨로지의 공통적인 특징은 복잡한 구조적 설명(예: 특정 신체 부위를 어떤 부분이 포함하고 포함되어 있는지 정의하거나 부분-서브파트 관계에 따라 질병을 전파함), 많은 수의 클래스, 용어 관리를 위한 과도한 분류 사용과 결과 용어의 방대한 양의 데이터에 적용을 포함한다. 따라서 OWL 2 EL은 비교적 표현이 풍부한 클래스 표현 언어를 가지고 있으며 공리에서 그 사용 방법에 제한이 없다. 또한 속성 체인을 포함하지만 역 속성을 제외하는 속성 표현은 상당한 표현력을 갖고 있다.

OWL 2 EL의 합리적인 사용은 분명히 바이오헬스 영역에 국한되지 않는다. 다른 프로필과 마찬가지로 OWL 2 EL은 영역에 독립적이다. 그러나 OWL 2 EL은 도메인과 어플리케이션이 구조적으로 복잡한 객체를 인식해야 할 때 빛을 발한다. 이러한 영역에는 시스템 구성, 제품 인벤토리와 많은 과학 영역이 포함된다.

부정과 분리 외에도 OWL 2 EL은 속성에 대한 보편적인 정량 조건(quantification)도 허용하지 않는다. 그러므로 “부자의 자식은 다 부자다”와 같은 명제를 취급할 수 없다. 더우기 모든 종류의 역할 역을 사용할 수 없기 때문에 parentOf와 childOf가 서로 역임을 지정할 수 있는 방법이 없다.

약어 EL은 소위 EL 기술 논리 계열([Pushing the EL Envelope](http://www.informatik.uni-bremen.de/~clu/papers/archive/ijcai05.pdf))이 프로필의 기초에 반영되었다. 그들은 주로 변수의 존재 조건(Extential quantification)를 제공하는 언어이다.

다음은 OWL 2 EL에서 사용할 수 있는 몇 가지 일반적인 모델링 기능을 사용한 예이다.

**함수 스타일 syntax**
```
 SubClassOf(
   :Father 
   ObjectIntersectionOf( :Man :Parent )
 )
 
 EquivalentClasses(
   :Parent 
   ObjectSomeValuesFrom(
     :hasChild 
     :Person
   )
 )
 
 EquivalentClasses( 
   :NarcisticPerson 
   ObjectHasSelf( :loves ) 
 )
 
 DisjointClasses( 
   :Mother 
   :Father 
   :YoungChild 
 )
 
 SubObjectPropertyOf( 
   ObjectPropertyChain( :hasFather :hasBrother ) 
   :hasUncle 
 )
 
 NegativeObjectPropertyAssertion( 
   :hasDaughter 
   :Bill 
   :Susan 
 )
```

## OWL 2 QL
OWL 2 QL은 표준 관계형 데이터베이스 기술(예: SQL)을 사용하여 클래스 공리에 비추어 쿼리를 확장하여 간단히 구현할 수 있다. 이는 RDBMS와 긴밀하게 통합될 수 있고 RDBMS의 안정성과 다중 사용자 기능의 이점을 누릴 수 있음을 의미한다. 또한 "데이터를 건드릴" 필요 없이 실제로 번역/전처리 계층으로 구현할 수 있다. 표현적으로 Entity-relationship과 UML 다이어그램의 주요 기능(적어도 함수적 제한이 있는 기능)을 나타낼 수 있다. 따라서 데이터베이스 스키마를 표현하고 쿼리 재작성을 통해 통합하는 데 모두 적합하다. 결과적으로 사용자가 다이어그램 기반 구문을 선호할 수 있지만 고급 데이터베이스 스키마 언어로 직접 사용할 수도 있다.

OWL 2 QL은 또한 RDFS에서 일반적으로 사용되는 많은 기능, 역 속성, 하위 속성 계층과 같은 작은 확장을 지원한다. OWL 2 QL은 클래스 공리를 비대칭적으로 제한한다. 즉, 상위 클래스로 사용할 수 없는 구성요소만 하위 클래스로 사용할 수 있다.

다른 구성 요소 중에서 OWL 2 QL은 클래스 표현 중 역할의 존재 조건을 허용하지 않는다. 즉 모든 사람에게 부모가 있다고 말할 수 있지만 모든 사람에게 여성 부모가 있는 것은 아니다. 또한 속성 체인 공리와 일치(equality)는 지원되지 않는다.

QL 약어는 이 프로필에서 쿼리 결과는 쿼리를 표준 관계형 쿼리 언어([Tractable Reasoning and Efficient Query Answering in Description Logics: The DL-Lite Family](http://www.springerlink.com/content/n17338715966v81h/))로 다시 작성하여 구현할 수 있다는 사실을 반영한다.

다음은 OWL 2 QL에서 사용할 수 있는 몇 가지 일반적인 모델링 기능을 사용한 예이다. 첫 번째 공리는 자녀가 없는 모든 사람은 그 사람이 부모인 첫 번째 사람이 존재하지 않는 사람이라고 할 수 있다.

**함수 스타일 syntax**
```
 SubClassOf( 
   :ChildlessPerson 
   ObjectIntersectionOf(
     :Person 
     ObjectComplementOf(
       ObjectSomeValuesFrom(
         ObjectInverseOf( :hasParent )
         owl:Thing
       )
     )
   )
 ) 
 
 DisjointClasses( 
   :Mother 
   :Father
   :YoungChild 
 )
 
 DisjointObjectProperties( 
   :hasSon 
   :hasDaughter 
 )
 
 SubObjectPropertyOf( 
   :hasFather 
   :hasParent
 )
```

## OWL 2 RL
OWL 2 RL 프로필은 많은 표현력을 희생하지 않으면서 확장 가능한 추론이 필요한 어플리케이션을 목표로 한다. 이는 효율성을 위해 언어의 완전한 표현력을 감내할 수 있는 OWL 2 어플리케이션과 OWL 2에 약간의 추가 표현력을 필요로 하는 RDF(S) 어플리케이션을 모두 수용하도록 설계되었다. 규칙 기반 기술을 사용하여 구현하고 이러한 구현의 기초로 사용할 수 있는 1차(first-order) 의미의 형태로 OWL 2 의미론의 부분적 공리를 제시한다.

RDF 기반 시맨틱에서 OWL 2 RL의 적절한 규칙 기반 구현을 임의의 RDF 그래프와 함께 사용할 수 있다. 결과적으로 OWL 2 RL은 특히 데이터가 추가 규칙에 의해 마사지되어야 하는 경우 RDF 데이터를 강화하는 데 이상적이다. 그러나 모델링 관점에서 이는 클래스 표현식으로 작업하는 것에서 멀어지게 한다. OWL 2 RL은 수퍼클래스 표현식에서 알려지지 않은 개체에 대해 (쉽게) 진술할 수 없도록 한다 (이 제한은 규칙의 특성에서 따른다). OWL 2 QL과 비교할 때 OWL 2 RL은 이미 데이터를 RDF로 마사지함으로써 RDF로 작업할 때 더 잘 작동한다.

다른 구성 요소 중에서 OWL 2 RL은 개체의 존재가 다른 개체의 존재를 강제하는 진술을 허용하지 않는다. 예를 들어 "모든 사람은 부모가 있습니다"라는 진술은 OWL RL에서 표현할 수 없다.

OWL 2 RL은 클래스 공리를 비대칭적으로 제한한다. 즉, 상위 클래스로 사용할 수 없는 구성 요소만 하위 클래스로 사용할 수 있다.

RL 약어는 이 프로필의 추론이 표준 규칙 언어([Description Logic Programs: Combining Logic Programs with Description Logic](http://www2003.org/cdrom/papers/refereed/p117/p117-grosof.html))를 사용하여 구현될 수 있다는 사실을 반영한다.

다음은 OWL 2 RL에서 사용할 수 있는 몇 가지 일반적인 모델링 기능을 사용하는 예이다. 다소 인위적인 첫 번째 공리는 여성인 Mary, Bill 및 Meg 각각에 대해 다음이 성립한다고 한다. 그녀는 최대 한 명의 자녀를 둔 부모이고 모든 자녀(있는 경우)는 여성이다.

**함수 스타일 syntax**
```
 SubClassOf(
   ObjectIntersectionOf(
     ObjectOneOf( :Mary :Bill :Meg )
     :Female
   )
   ObjectIntersectionOf(
     :Parent
     ObjectMaxCardinality( 1 :hasChild )
     ObjectAllValuesFrom( :hasChild :Female )
   )
 )
 
 DisjointClasses( 
   :Mother 
   :Father 
   :YoungChild 
 )
 
 SubObjectPropertyOf( 
   ObjectPropertyChain( :hasFather :hasBrother ) 
   :hasUncle 
 )
```
