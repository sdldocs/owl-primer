# 데이터 타입의 고급 사용(Advanced Use of Datatypes)
[데이터타입](./basic-modeling.md#datatypes)에서 개체 속성이 다른 도메인 개체에 연결되는 것과 마찬가지로 기본적으로 데이터 타입 속성을 데이터 값에 연결함으로써 개체에 수에 대한 정보(numerical information)를 기술할 수 있다는 것을 설명하였다. 사실, 이러한 유사점은 데이터 타입 사용의 고급 기능으로 확장된다.

우선, 데이터 값은 데이터 타입으로 그룹화되며 [데이터 타입](./basic-modeling.md#datatypes)에서 데이터 타입 속성에 대한 범위 제한을 사용하여 이 속성에 연결할 수 있는 값의 종류를 나타내는 방법을 보였다. 또한 기존 데이터 타입을 제한하거나 결합하여 새로운 데이터 타입을 표현하고 정의할 수 있다. 데이터 타입을 XML 스키마 데이터 타입([W3C XML Schema Definition Language (XSD) 1.1 Part 2: Datatypes](http://www.w3.org/TR/xmlschema11-2/))에서 차용한 *패싯(facet)*을 통해 제한할 수 있다. 다음 예는 데이터 타입 정수를 0에서 150 사이의 값으로 제한하여 사람의 나이에 대한 새 데이터 타입을 정의한다.

**함수 스타일 syntax**
```
DatatypeDefinition(
   :personAge
   DatatypeRestriction( xsd:integer           
     xsd:minInclusive "0"^^xsd:integer
     xsd:maxInclusive "150"^^xsd:integer
   )
) 
```

마찬가지로 데이터 타입은 보집합, 교집합 및 합집합을 이용하여 클래스처럼 결합될 수 있다. 따라서 데이터 타입 `minorAge`를 이미 정의했다고 가정하고 `personAge`에서 `minorAge`의 모든 데이터 값을 제외하여 `majorAge` 데이터 타입을 정의할 수 있다.

**함수 스타일 syntax**
```
DatatypeDefinition(
   :majorAge
   DataIntersectionOf(
     :personAge 
     DataComplementOf( :minorAge ) 
   ) 
) 
```

또한, 포함된 데이터 값을 열거하여 새로운 데이터 타입을 생성할 수 있다.

**함수 스타일 syntax**
```
DatatypeDefinition(
   :toddlerAge
   DataOneOf( "1"^^xsd:integer "2"^^xsd:integer ) 
) 
```

[속성 특성](./advanced-use-properties#property-characteristics)에서 객체 속성을 특성화하는 방법을 보았다. 그 중 일부는 데이터 타입 속성에도 사용할 수 있다. 예를 들어, `hasAge` 데이터 타입 속성을 함수적(functional)인 것으로 특성화하여 모든 사람이 하나의 나이만 가지고 있음을 표현할 수 있다.

**함수 스타일 syntax**
```
FunctionalDataProperty( :hasAge )
```

데이터 타입 속성에 대한 제한으로 새 클래스를 정의할 수 있다. 다음 예는 13세에서 19세 사이의 모든 개체로 `Teenager` 클래스을 정의한다.

**함수 스타일 syntax**
```
SubClassOf(
   :Teenager
   DataSomeValuesFrom( :hasAge
     DatatypeRestriction( xsd:integer
       xsd:minExclusive "12"^^xsd:integer
       xsd:maxInclusive "19"^^xsd:integer
     )
   )
)
```
