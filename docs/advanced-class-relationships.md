# 고급 클래스 관계(Advanced Class Relationships)
이전 섹션에서 클래스를 이름을 지닌 "불투명한" 것으로 처리했다. 그것들을 사용하여 개체를 특성화하고 하위 클래스 또는 단절 진술을 통해 다른 클래스와 관련시켰다.

이제 새 클래스를 정의하기 위하여 명명된 클래스, 속성과 개체를 빌딩 블록으로 사용하는 방법을 보인다.

## 복합 클래스(Complex Classes)
지금까지 설명한 언어 요소를 통해 간단한 온톨로지로 모델링할 수 있다. 보다 복잡한 지식을 표현하기 위해 OWL은 논리적 클래스 생성자를 제공한다. 특히 OWL은 논리 연산자 and, or와 not의 언어 요소를 지원한다. OWL 용어 (클래스) *교집합*, *합집합*과 *보집합*는 집합 이론에서 왔다. 이러한 생성자는 원자 클래스(즉, 이름이 있는 클래스)를 복잡한 클래스로 결합한다.

두 클래스의 *교집합*은 정확히 두 클래스의 인스턴스인 개체로 구성됩니다. 다음 예제에서 `Mother` 클래스가 `Woman`과 `Parent`의 인스턴스인 객체로 정확히 구성되어 있음을 나타낸다.

**함수 스타일 syntax**
```
EquivalentClasses(
   :Mother 
   ObjectIntersectionOf( :Woman :Parent )
) 
```

이것에서 도출할 수 있는 추론의 예는 Mother 클래스의 모든 인스턴스가 Parent 클래스에도 있다는 것이다.

두 클래스의 *합집합*에는 이러한 클래스 중 적어도 하나에 포함된 모든 개체가 포함된다. 따라서 모든 부모의 클래스는 `Mother`와 `Father` 클래스의 합집합이다.

**함수 스타일 syntax**
```
EquivalentClasses(
   :Parent 
   ObjectUnionOf( :Mother :Father )
) 
```

클래스의 *보집합*은 논리적 부정에 해당한다. 클래스 자체의 구성원이 아닌 객체로 정확히 구성된다. 자녀가 없는 사람에 대한 다음 정의는 클래스 보집합을 사용하고 클래스 생성자가 중첩될 수 있음을 보인다.

**함수 스타일 syntax**
```
EquivalentClasses(
   :ChildlessPerson 
   ObjectIntersectionOf(
     :Person 
     ObjectComplementOf( :Parent )
   )
) 
```

위의 모든 예는 새 클래스를 다른 클래스의 조합으로 *정의*하기 위해 클래스 생성자를 사용하는 방법을 보여주었다. 그러나 물론, 클래스에 대하여 필요하지만 충분하지 않은 조건을 나타내기 위해 SubClassOf 문과 함께 클래스 생성자를 사용할 수도 있다. 다음 진술은 모든 할아버지가 남자이자 부모라는 것을 나타낸다 (반면 그 반대는 반드시 참은 아니다).

**함수 스타일 syntax**
```
SubClassOf( 
   :Grandfather 
   ObjectIntersectionOf( :Man :Parent )
)
```

일반적으로 복합 클래스(complex classes)는 명명된 클래스가 있는 모든 위치에서 사용할 수 있으므로 클래스 선언(assertion)에서도 사용할 수 있다. 이는 Jack이 사람이지만 부모가 아니라고 하는 다음 예에서 볼 수 있다.

**함수 스타일 syntax**
```
 ClassAssertion(
   ObjectIntersectionOf(
     :Person 
     ObjectComplementOf( :Parent )
   ) 
   :Jack
 )
```

## 속성 제약(Property Restrictions)
속성 제한은 복합 클래스에 대한 또 다른 타입의 논리 기반 생성자를 제공한다. 이름에서 알 수 있듯이 속성 제한은 속성과 관련된 생성자를 사용한다.

*존재 조건(existential quantification)*이라는 속성 제한은 클래스를 특정 속성을 통해 다른 클래스의 인스턴스인 다른 개체과 연결된 모든 개체의 집합을 정의한다. 이는 부모의 클래스를 `hasChild` 속성으로 다른 사람과 연결된 개체의 클래스로 정의하는 다음 예에서 가장 잘 설명되고 있다.

**함수 스타일 syntax**
```
 EquivalentClasses(
   :Parent 
   ObjectSomeValuesFrom( :hasChild :Person )
 )
```

이는 `Parent`의 모든 인스턴스에 대해 최소한 하나의 자식이 있고 해당 자식이 `Person` 클래스의 구성원이라는 기대가 있음을 뜻한다. 이것은 *불완전한* 지식을 포착하는 데 유용하다. 예를 들어 Sally는 Bob이 부모라고 말하므로 이름을 모르더라도 적어도 한 명의 자녀가 있다고 추론할 수 있다. 존재 조건의 사용을 위한 자연어 지표는 "일부" 또는 "하나"와 같은 단어이다.

*w전체 조건(Universal quantification)*이라는 또 다른 속성 제한은 관련된 모든 개인이 지정된 클래스의 인스턴스여야 하는 개인 클래스를 설명하는 데 사용된다. 다음 문장을 사용하여 누군가의 자녀가 모두 행복한 사람이라면 누군가가 행복한 사람이라는 것을 나타낼 수 있다.

**함수 스타일 syntax**
```
 EquivalentClasses(
   :HappyPerson 
   ObjectAllValuesFrom( :hasChild :HappyPerson )
 )
```

이 예 또한 OWL 문이 특정 방식으로 자기 참조적일 수 있음을 보여준다. `HappyPerson` 클래스를 동등성 문의 양쪽에 사용하였다.

속성 제한의 사용은 "모델링 초보자"에게 개념적 혼란을 일으킬 수 있다. 경험에 따르면 자연어 문장을 논리적 공리로 번역할 때 존재 조건이 훨씬 더 자주 나타난다. 전체 조건 사용을 위한 자연어 지표는 "만", "배타적으로" 또는 "아무것도 아닌 것"과 같은 단어들이다.

전체 역할(universal role) 제한과 관련하여 한 가지 특별한 오해가 있다. 예를 들어, 위의 행복 공리를 보면, 직관적으로는 사람이 행복하기 위해서는 적어도 한 명의 행복한 자녀가 있어야 함을 시사한다. 그러나 이것은 사실이 아니다. `hasChild` 속성의 "시작점(starting point)"이 아닌 개체는 `hasChild`에 대한 전체 조건으로 정의된 클래스의 클래스 구성원이다. 따라서 위의 진술에 따르면 자녀가 없는 모든 사람은 행복할 자격이 있다. 앞서 언급한 의도를 공식화하려면 진술을 다음과 같이 이해하여야 한다.

**함수 스타일 syntax**
```
 EquivalentClasses(
   :HappyPerson 
   ObjectIntersectionOf(
     ObjectAllValuesFrom( :hasChild :HappyPerson )
     ObjectSomeValuesFrom( :hasChild :HappyPerson )
   )
 )
```

이 예는 또한 속성 제한이 복합 클래스와 중첩될 수 있는 방법을 보인다.

또한 한 특정 개체와 관련된 개인의 클래스를 설명하는 데 속성 제한을 사용될 수 있다. 예를 들어 John의 자식 클래스를 정의할 수 있다.

**함수 스타일 syntax**
```
 EquivalentClasses( 
   :JohnsChildren 
   ObjectHasValue( :hasParent :John )
 )
```

개체가 속성에 의해 연결되는 특수한 경우, 개체는 자신과 연결될 수 있다. 다음 예는 모든 나르시스트가 자신을 사랑한다는 것을 표현하는 방법을 보인다.

**함수 스타일 syntax**
```
 EquivalentClasses(
   :NarcisticPerson 
   ObjectHasSelf( :loves ) 
 )
```

## 속성 카디널리티 제한(Property Cardinality Restrictions)
전체 조건을 사용하여 누군가의 모든 자녀에 대해 말할 수 있는 반면 존재 조건을 사용하면 (적어도) 그 중 하나를 참조할 수 있다. 그러나 제한에 관련된 개체의 수를 지정할 수 있다. 실제로 어린이 수에 따라 클래스를 구성할 수 있다. 다음 예에서 `John`은 최대 4명의 자녀가 있음을 나타낸다.

**함수 스타일 syntax**
```
 ClassAssertion(
   ObjectMaxCardinality( 4 :hasChild :Parent ) 
   :John
 )
```

이 진술을 통해 `John`은 자신이 부모가 아닌 더 많은 자녀를 임의로 더 많이 가질 수 있다.

마찬가지로 `John`이 부모인 자녀가 두 명 이상 있는 개인 클래스의 인스턴스라고 말하여 최소 수를 선언하는 것도 가능하다.

**함수 스타일 syntax**
```
 ClassAssertion(
   ObjectMinCardinality( 2 :hasChild :Parent ) 
   :John
 )
```

부모인 `John`의 자녀 수를 정확히 알고 있는 경우 다음과 같이 지정할 수 있다.

**함수 스타일 syntax**
```
 ClassAssertion( 
   ObjectExactCardinality( 3 :hasChild :Parent ) 
   :John
 ) 
```

카디널리티 제한에서 이를 클래스에 제공하는 것은 선택 사항이다. `John`의 모든 자녀 수에 대해 다음과 같이 작성할 수 있다.

```
 ClassAssertion(
   ObjectExactCardinality( 5 :hasChild ) 
   :John
 )
```

## 개체 열거(Enumeration of Individuals)
클래스를 기술하는 매우 간단한 방법은 모든 인스턴스를 열거하는 것이다. OWL은 이렇게 할 수 있다. 즉, 생일 손님 클래스를 만들 수 있다.

```
 EquivalentClasses(
   :MyBirthdayGuests
   ObjectOneOf( :Bill :John :Mary)
 )
```

이 공리는 [클래스와 인스탄스](basic-modeling.md#classes-instances)에 설명된 것처럼 `Bill`, `John`과 `Mary`의 클래스 멤버십을 단순히 주장하는 것보다 더 많은 정보를 제공한다. 또한 `Bill`, `John`과 `Mary`만 `MyBirthdayGuests`의 *오직* 유일한 구성원이라고 규정한다. 따라서 이러한 방식으로 정의된 클래스를 *닫힌 클래스(closed classes)* 또는 열거 집합(enumerated set)이라고 하는 경우가 있다. 이제 `Jeff`를 `MyBirthdayGuests`의 인스턴스로 하면 `Jeff`는 위의 세 사람 중 한 사람이어야 한다.
