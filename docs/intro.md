# 개요(Introduction)
W3C OWL 2 웹 온톨로지 언어(OWL)는 복잡한 지식을 사물, 사물 그룹 및 사물 간의 관계로 풍부하게 나타낼 수 있도록 설계된 시맨틱 웹 언어이다. OWL은 OWL로 표현된 지식이 해당 지식의 일관성을 확인하거나 암시적 지식을 명시적으로 만들기 위해 컴퓨터 프로그램이 추론할 수 있는 계산 논리 기반 언어이다. 온톨로지로 알려진 OWL 문서는 World Wide Web에 게시될 수 있으며 다른 OWL 온톨로지를 참조하거나 참조될 수 있다. OWL은 RDF[[Resource Description Framework (RDF): Concepts and Abstract Syntax](https://sdldocs.gitlab.io/rdf/)]와 SPARQL[[SPARQL Query Language for RDF](https://sdldocs.gitlab.io/sparql/)]을 포함하는 W3C의 [시맨틱 웹](http://www.w3.org/2001/sw/) 기술 스택의 일부이다.

문서의 주요 목표는 OWL, 강점 및 약점에 대한 통찰력을 개발하는 데 도움을 주고자 하는 것이다. 문서의 핵심은 실행 예를 통해 OWL의 대부분의 언어 기능을 소개하는 것이다. 문서에 있는 대부분의 예는 [샘플 온톨로지](sample-ontology.md)에 수록된 것이다. 이 샘플 온톨로지는 그 자체로 좋은 온톨로지의 예이기 보다 이해할 수 있도록 OWL의 주요 언어 기능을 설명하는 것을 목표로 작성되었다.

## 문서 가이드
이 문서는 OWL 2에 대한 초기 이해를 제공하기 위한 것이다. 특히 아직 주제에 익숙하지 않은 사람들이 이해할 수 있어야 한다. 따라서 우리는 [지식 모델링 - 기본 개념](./modeling-knowledge.md)에서 지식 표현에 대한 몇 가지 매우 기본적인 개념을 제공하고 OWL 2에서 사용되는 용어와 어떻게 관련되는지 설명하기 전에 [OWL-2란?](./what.md)에서 OWL 2의 특성에 대한 전체적인 소개를 시작한다. 지식 표현과 추론에 익숙한 독자는 OWL 2 용어에 익숙해지기 위해 [지식 모델링 - 기본 개념](./modeling-knowledge.md)만 읽는 것을 추천한다.

이후 섹션들은 OWL이 제공하는 대부분의 언어 기능에 대해 아주 기본적인 것부터 시작해서 더 복잡한 것까지 설명한다. [클래스, 속성과 개체 - 기본 모델링](./basic-modeling.md)에서는 [고급 클래스 관계](./advanced-class-relationships.md)에서 복잡한 클래스를 소개하기 전에 OWL 2의 기본 모델링 기능을 제시하고 논의한다. [속성의 고급 사용](./advanced-use-properties.md)에서는 속성에 대한 고급 모델링 기능을 다룬다. [데이터 타입의 고급 사용](./advanced-use-datatypes.md)에서는 데이터 타입에 관련된 고급 모델링에 중점을 둔다. [정보와 주석 문서화](./document-info-annotations.md)에서는 주로 온톨로지 관리 목적으로 사용되는 추가 논리적 기능으로 마무리한다.

[OWL 2 DL과 OWL 2 Full](./owl2dl-full.md)에서는 OWL의 두 가지 의미론적 관점인 OWL 2 DL과 OWL 2 Full 간의 차이점을 설명하고 [OWL 2 프로파일](./owl2-profile.md)에서는 프로필이라고 하는 OWL 2의 다루기 쉬운 세 가지 하위 언어를 설명한다. OWL 2에 대한 도구 지원은 [OWL 도구](./owl-tools.md)에서 다룬다.

마지막으로 [부록 - 완전한 온톨로지 샘플](./sample-ontology.md)은 이 문서에서 사용된 예제 온톨로지를 나열한다.

OWL 2 언어 기능의 포괄적인 목록은 구문과 예에 관련된 해당 문서의 해당 섹션에 대한 링크를 제공하는 OWL 2 빠른 참조 가이드([OWL 2 Web Ontology Language: Quick Reference Guide (Second Edition)](http://www.w3.org/TR/owl2-quick-reference/))를 참조한다.

이미 OWL 1에 익숙한 독자를 위해 OWL 2의 새로운 기능과 해석([OWL 2 Web Ontology Language: New Features and Rationale (Second Edition)](http://www.w3.org/TR/owl2-new-features/))은 OWL 2에서 변경된 사항에 대한 포괄적인 개요를 제공한다.

## OWL 구문 <a id="owl-syntax"></a>
OWL은 Semantic Web에서 사용되는 언어이므로 OWL의 이름은 IRI(International Resource Identifier)([RFC 3987]())이다. IRI가 길기 때문에 OWL로 작성하기 위해 약어 메커니즘을 사용하는 경우가 많다. 이러한 약어가 작동하는 방식은 OWL 온톨로지를 인코딩하는 데 사용할 수 있는 각 구문 형식에 따라 다르지만 이 문서의 예에서는 일반적으로 이러한 세부 사항을 몰라도 이해할 수 있다. 내임스페이스와 관련 메커니즘에 대한 적절한 선언을 아래 [온톨로지 관리](./document-info-annotations.md#ontology-management)에서 추가로 제공한다. 

다양한 용도로 사용되는 OWL에 사용할 수 있는 다양한 구문이 있습니다. Functional-Style 구문([OWL 2 Web Ontology Language: Structural Specification and Functional-Style Syntax (Second Edition)](http://www.w3.org/TR/owl2-syntax/))은 사양 기술을 목적으로 쉽게 설계되었으며 API와 추론기같은 OWL 2 도구의 구현을 위한 기반을 제공한다. OWL에 대한 RDF/XML 구문은 OWL 구성([OWL 2 Web Ontology Language: Mapping to RDF Graphs (Second Edition)]( http://www.w3.org/TR/owl2-mapping-to-rdf/))에 대한 특정 번역일 뿐이다. 이것은 모든 OWL 2 도구가 지원해야 하는 유일한 구문이다. Manchester 구문([OWL 2 Web Ontology Language: Manchester Syntax (Second Edition)](http://www.w3.org/TR/owl2-manchester-syntax/))은 논리학자가 아닌 사용자가 읽기 쉽게 설계된 OWL 구문이다. OWL XML 구문은 XML 스키마([OWL 2 Web Ontology Language: XML Serialization (Second Edition)](http://www.w3.org/TR/owl2-xml-serialization/))에 의해 정의된 OWL용 XML 구문이다. OWL의 다른 구문 사이를 번역할 수 있는 도구가 있다. 많은 구문 형식에서 OWL 언어 구성은 IRI로도 표시되며 예에서와 같이 축약된 형식을 사용하려면 일부 선언이 필요할 수 있다. 필요한 세부 사항은 [온톨로지 관리](./document-info-annotations.md#ontology-management)에서 찾을 수 있다.

