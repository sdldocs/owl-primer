# 속성의 고급 사용(Advanced Use of Properties)
지금까지 클래스 표현의 빌딩 블록으로만 사용되었던 클래스와 속성에 집중했다. 이제부터 OWL 2 속성이 제공하는 다른 모델링 기능에 대해 살펴보겠다.

## 속성 특성(Property Characteristics) <a id="property-characteristics"></a>
때때로 한 속성의 방향을 변경하여(즉, 반전하여) 다른 속성을 얻을 수 있다. 예를 들어 `hasParent` 속성은 `hasChild`의 역 속성으로 정의할 수 있다.

**함수 스타일 syntax**
```
InverseObjectProperties( :hasParent :hasChild ) 
```

예를 들어 이는 임의의 개체 A와 B에 대해 A가 `hasChild` 속성으로 B에 연결되어 있고 B도 A에 `hasParent` 속성으로 상호 연결되어 있음을 추론할 수 있다. 그러나 클래스 표현 내에서 사용하려는 경우 속성의 역에 이름을 명시적으로 할당할 필요가 없다. Orphan 클래스의 정의에 새로운 `hasParent` 속성을 사용하는 대신 `hasChild-inverse`로 직접 참조할 수 있다.

**함수 스타일 syntax**
```
 EquivalentClasses(
   :Orphan
   ObjectAllValuesFrom(
     ObjectInverseOf( :hasChild )
     :Dead
   )
 ) 
```

어떤 경우에는 속성과 그 역이 일치하거나 속성의 방향이 중요하지 않을 수 있다. 예를 들어 `hasSpouse` 속성으로 B와 A를 연결하는 경우 정확히 A와 B를 이로 연결하는 것과 같다. 명백한 이유로 이런 특성을 갖는 속성을 *대칭(symmetric)*이라고 하며 다음과 같이 지정할 수 있다.

**함수 스타일 syntax**
```
SymmetricObjectProperty( :hasSpouse )
```

반면에 한 속성으로 A를 B에 연결하는 경우 B가 A에 연결되지 않는다는 것을 의미하는 *비대칭(asymmetric)*일 수도 있다. 분명히(시간 여행으로 인한 역설적 시나리오를 제외하면) `hasChild` 속성의 경우이며 다음과 같이 표현할 수 있다.

**함수 스타일 syntax**
```
AsymmetricObjectProperty( :hasChild )
```

비대칭(asymmetric)인 것은 대칭이 아닌(non-symmetric) 것보다 훨씬 더 강력한 개념이다. 마찬가지로 대칭은 비대칭이 아닌 것보다 훨씬 더 강력한 개념이다.

이전에는 하위 속성을 하위 클래스와 유사하게 여겼다. 클래스 단절의 개념을 속성으로 옮기는 것도 의미가 있음이 밝혀졌다. 두 속성을 상호 연결한 두 개체가 없으면 두 속성은 단절된 것이다. 관습법에 따라 우리는 부모와 자식간 결혼이 일어날 수 없다고 말할 수 있다.

**함수 스타일 syntax**
```
DisjointObjectProperties( :hasParent :hasSpouse )
```

속성은 *재귀적(reflexive)*일 수도있다. 이러한 속성은 모든 것을 자신과 관련시킨다. 다음 예에서 모든 사람은 자신을 친척으로 간주한다.

**함수 스타일 syntax**
```
ReflexiveObjectProperty( :hasRelative )
```

이것이 반드시 재귀 속성과 관련된 모든 두 개체가 동일하다는 것을 의미하지는 않는다.

속성은 더 나아가 *비반사적(irreflexive)*일 수 있다. 즉, 어떤 개인도 그러한 역할에 의해 자신과 관련될 수 없음을 의미한다. 그 누구도 자신의 부모가 될 수 없음을 단순히 명시한 다음이 대표적인 예이다.

**함수 스타일 syntax**
```
IrreflexiveObjectProperty( :parentOf )   
```

다음으로, `hasHusband` 속성을 고려하면, 모든 사람은 한 명의 남편만 가질 수 있으므로(예를 위해 이를 당연하게 여김) 모든 개인은 `hasHusband` 속성으로 최대 한 명의 다른 개인과 연결할 수 있다. 이러한 종류의 속성을 `함수적(functional)`이라고 하며 다음과 같이 기술된다.

**함수 스타일 syntax**
```
FunctionalObjectProperty( :hasHusband )
```

이 진술은 모든 개체가 남편을 가질 것을 요구하지 않는다는 점에 유의하여야 한다. 단지 남편이 한 명보다 많을 수 없다고 명시되어 있다. 또한 Mary의 남편이 James이고 다른 하나가 Mary의 남편이 Jim이라는 진술이 추가로 있다면 Jim과 James가 동일한 개체를 참조해야 함을 유추할 수 있다.

주어진 속성의 역함수가 함수적임을 나타낼 수도 있다.

**함수 스타일 syntax**
```
InverseFunctionalObjectProperty( :hasHusband )
```

이는 개체가 최대 한 명의 다른 개체의 남편이 될 수 있음을 나타낸다. 이 예는 또한 일부다처제 상황에서 전자는 유효하지만 후자가 유효하지 않은 것처럼 기능과 역 기능의 차이점을 나타낸다.

이제 A가 B의 직계 후손일 때 개인 A와 B를 연결하는 `hasAncestor` 속성을 살펴본다. 분명히 `hasParent` 속성은 `hasAncestor`의 "특수한 경우"이며 하위 속성으로 정의할 수 있다. 그래도 부모의 부모(와 부모의 부모의 부모)를 "자동으로" 포함하는 것이 좋다. 이는 `hasAncestor`를 *전이(transitive)* 속성으로 정의하여 수행할 수 있다. 전이 속성은 어떤 개체 B에 대해 A와 B, B와 C를 연결할 때마다 두 개체 A와 C도 연결한다.

**함수 스타일 syntax**
```
TransitiveObjectProperty( :hasAncestor )
```

## 속성 체인(Property Chains)
이전 섹션의 마지막 예는 `hasParent` 속성 체인이 있을 때마다 `hasAncestor` 속성이 있음을 암시했지만, 이것 대신 더 구체적인 `hasGrandparent` 속성을 정의하고 싶을 수 있다. 기술적으로 이는 `hasGrandparent`가 정확히 두 개의 `hasParent` 속성 체인으로 연결된 모든 개체를 연결하는 것을 의미한다. 이전의 `hasAncestor` 예와 달리, `hasParent`가 `hasGrandparent`의 특별한 경우가 되는 것을 원하지 않으며 `hasGrandparent`가 증조부모 등을 참조하는 것을 원하지 않는다. 이러한 모든 체인은 다음과 같이 `hasGrandparent` 속성에 의해 확장되어야 한다고 표현할 수 있다.

**함수 스타일 syntax**
```
SubObjectPropertyOf( 
   ObjectPropertyChain( :hasParent :hasParent ) 
   :hasGrandparent 
)
```

## 키(Keys)
OWL 2에서 (데이터 또는 객체) 속성의 컬렉션을 클래스 표현의 키로 사용할 수 있다. 이는 인스턴스와 관련하여 클래스 표현의 각 명명된 인스턴스가 이러한 속성 값 집합으로 고유하게 식별된다는 것을 의미한다.

다음 간단한 예는 사회 보장 번호로 사람을 식별하는 것이다.

**함수 스타일 syntax**
```
HasKey( :Person () ( :hasSSN ) )
```
