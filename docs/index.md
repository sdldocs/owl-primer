# OWL Primer <sup>[1](#footnote_1)</sup>

OWL 2 Web Ontology Language(비공식적으로 OWL 2)는 공식적으로 정의된 의미를 가진 Semantic Web을 위한 온톨로지 언어이다. OWL 2 온톨로지는 클래스, 속성, 개체 및 데이터 값을 제공하며 Semantic Web 문서로 저장된다. OWL 2 온톨로지는 RDF로 작성된 정보와 함께 사용될 수 있으며, OWL 2 온톨로지 자체는 주로 RDF 문서로 교환된다. [OWL 2 문서 개요](http://www.w3.org/TR/2012/REC-owl2-overview-20121211/)는 OWL 2의 전반적인 상태를 설명하며 다른 OWL 2 문서보다 먼저 읽어야 한다.

이 문서는 OWL 2를 알기 쉽게 소개한다. 여기에는 다른 분야 사람들을 위한 오리엔테이션, OWL 2를 사용하여 첫 번째 간단한 정보를 표현한 다음 더 복잡한 정보를 표현하는 방법, OWL 2가 온톨로지를 관리하는 방법, 마지막으로 OWL 2의 다양한 하위 언어간의 차이점을 포함하고 있다. 

문서의 목차는 다음과 같다.

- [개요](intro.md)
- [OWL-2란?](what.md)
- [지식 모델링 - 기본 개념](modeling-knowledge.md)
- [클래스, 속성과 개체 - 기본 모델링](basic-modeling.md)
- [고급 클래스 관계](advanced-class-relationships.md)
- [속성의 고급 사용](advanced-use-properties.md)
- [데이터 타입의 고급 사용](advanced-use-datatypes.md)
- [정보와 주석 문서롸](document-info-annotations.md)
- [OWL 2 DL과 OWL 2 Full](owl2dl-full.md)
- [OWL 2 프로파일](owl2-profile.md)
- [OWL 도구](owl-tools.md)
- [다음?](what-next.md)
- [부록 - 완전한 온톨로지 샘플](sample-ontology.md)

<a name="footnote_1">1</a>: 이 페이지는 [OWL 2 Web Ontology Language Primer (Second Edition)](https://www.w3.org/TR/owl2-primer/)을 편역한 것임.

To do:

- [OWL 2 Web Ontology Language: RDF-Based Semantics (Second Edition)](http://www.w3.org/TR/owl2-rdf-based-semantics/)
- [OWL 2 Profiles: An Introduction to Lightweight Ontology Languages](http://korrekt.org/page/OWL_2_Profiles)
- [Protégé](http://protege.stanford.edu/)
- [OWL API](http://owlapi.sourceforge.net/)
- [Foundations of Semantic Web Technologies](https://www.semantic-web-book.org/)
- [OWL 2 Web Ontology Language: Structural Specification and Functional-Style Syntax (Second Edition)](http://www.w3.org/TR/owl2-syntax/)
- [OWL 2 Web Ontology Language: Mapping to RDF Graphs (Second Edition)](http://www.w3.org/TR/owl2-mapping-to-rdf/)
