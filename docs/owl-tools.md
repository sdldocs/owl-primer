# OWL 도구(OWL Tools)
OWL 온톨로지에서의 작업을 위해서는 도구 지원이 필수적이다. 기본적으로 온톨로지 라이프사이클의 두 가지 주요 단계를 다루는 두 가지 타입의 도구가 있다. 온톨로지 편집기는 온톨로지를 만들고 편집하는 데 사용되는 반면 추론기는 암시적 지식에 대해 온톨로지에 쿼리하는 데 사용된다. 즉 그들은 질의의 한 진술이 온톨로지의 논리적 결과 여부를 결정한다.

현재 가장 널리 사용되는 OWL 편집기는 Stanford University에서 개발한 무료 오픈 소스 편집 프레임워크인 [Protégé](http://protege.stanford.edu/)이다. 개방형 플러그인 구조 덕분에 특수 목적의 온톨로지 편집 구성 요소들이 쉽게 통합될 수 있다. 다른 편집기로는 TopQuadrant의 상용 [TopBraid Composer](http://www.topquadrant.com/products/TB_Composer.html)와 오픈 소스 시스템으로 [SWOOP](http://code.google.com/p/swoop/)과 [NeOn-Toolkit](http://www.neon-toolkit.org/)이 있다.

지원되는 추론 기능의 적용 범위 측면에서 차별되는 OWL DL를 위한 추론기들이 있다. 이들 중 일부는 현재 OWL 2 준수를 계획하고 있으며 해당 구현이 진행 중이다. [Test Suite Status](http://www.w3.org/2007/OWL/wiki/Test_Suite_Status) 문서는 아래에 언급된 추론 중 일부가 테스트 케이스를 준수하는 정도를 나열한다.

OWL DL 내 추론 중에서 가장 눈에 띄는 시스템은 맨체스터 대학의 [Fact++](http://owl.cs.manchester.ac.uk/fact++/), Oxford University Computing Laboratory의 [Hermit](http://hermit-reasoner.com/), Clark & Parsia, LLC의 [Pellet](http://clarkparsia.com/pellet)과 Racer Systems의 [RacerPro](http://www.racer-systems.com/)들 이다.

모든 OWL DL을 지원하는 것을 목표로 하는 범용 추론기 외에도 OWL의 다루기 쉬운 프로필에 맞춤화된 추론 시스템이 있다. Dresden University of Technology의 [CEL](http://lat.inf.tu-dresden.de/systems/cel/)은 OWL EL을 지원한다. Sapienza Università di Roma의 [QuOnto](http://www.dis.uniroma1.it/~quonto/)는 OWL QL을 지원한다. [ORACLE 11g](http://www.oracle.com/technology/tech/semantic_technologies/index.html)는 OWL RL을 지원한다.

오픈 소스 [OWL API](http://owlapi.sourceforge.net/)는 현재 OWL을 중심으로 가장 중요한 개발 도구로서 다소 두드러진 역할을 하고 있다.

이 문서가 작성될 당시에는 여러 OWL 도구가 개발 중이었으므로 현재 개요는 최신 개요가 아니라 이 개발의 스냅샷으로 보아야 한다. OWL 도구의 광범위한 목록이 [semanticweb.org](http://semanticweb.org/wiki/Tools)와 [ESW-Wiki](http://esw.w3.org/topic/SemanticWebTools)에 있다.
