# OWL 2 DL과 OWL 2 Full(OWL 2 DL and OWL 2 Full)

직접 의미론([OWL 2 Web Ontology Language: Direct Semantics (Second Edition)](http://www.w3.org/TR/owl2-direct-semantics/))과 RDF 기반 의미론[OWL 2 Web Ontology Language: RDF-Based Semantics (Second Edition)](http://www.w3.org/TR/owl2-rdf-based-semantics/)이라고 하는 OWL 2의 온톨로지에 의미를 할당하는 두 가지 대안이 있다. Direct Semantics는 OWL 2의 OWL 2 DL 부분 집합에 있는 온톨로지에 적용될 수 있으며, OWL 2 기능형 구문 문서([OWL 2 Web Ontology Language: Structural Specification and Functional-Style Syntax (Second Edition)](http://www.w3.org/TR/owl2-syntax/))에 정의되어 있다. OWL 2 DL에 없는 온톨로지는 종종 *OWL 2 Full*에 있다고 하며 RDF 기반 시맨틱 하에서만 해석될 수 있다. 비공식적으로 *OWL 2 DL*이라는 용어는 Direct Semantics를 사용하여 해석되는 OWL 2 온톨로지를 지칭하는 데 자주 사용되지만 RDF 기반 Semantics으로 OWL 2 DL 온톨로지를 해석할 수도 있다.

Direct Semantics([OWL 2 Web Ontology Language: Direct Semantics (Second Edition)](http://www.w3.org/TR/owl2-direct-semantics/))은 기술 논리([The Description Logic Handbook: Theory, Implementation, and Applications, second edition](https://dl.kr.org/)) 스타일로 OWL 2에 대한 의미를 제공한다. RDF 기반 의미론[OWL 2 Web Ontology Language: RDF-Based Semantics (Second Edition)](http://www.w3.org/TR/owl2-rdf-based-semantics/)은 RDFS([RDF Semantics](http://www.w3.org/TR/rdf-mt/))에 대한 의미론의 확장이며 OWL 2 온톨로지를 RDF 그래프로 보는 것을 기반으로 한다.

온톨로지를 생각할 때 이 두 의미의 차이는 일반적으로 아주 미미하다. 실제로, OWL 2 DL 온톨로지가 주어지면 Direct Semantics을 사용하여 도출된 많은 추론은 RDF 기반 의미론에서도 유효한 추론으로 남는다. RDF 기반 의미론 문서 RDF 기반 의미론([OWL 2 Web Ontology Language: RDF-Based Semantics (Second Edition)](http://www.w3.org/TR/owl2-rdf-based-semantics/))의 [섹션 7.2](http://www.w3.org/TR/2012/REC-owl2-rdf-based-semantics-20121211/#Correspondence_Theorem)에 있는 대응 정리를 참조한다. 두 가지 주요 차이점은 Direct Semantics 주석 아래에는 형식적인 의미가 없고 RDF 기반 의미 체계에는 세상에 대한 RDF 관점에서 발생하는 몇 가지 추가 추론이 있는 것이다.

개념적으로, OWL 2 DL(Direct Semantics에서)과 OWL 2 Full(RDF-Based Semantics에서)의 차이점을 두 가지로 생각할 수 있다.

- OWL 2 DL은 OWL 2 Full의 구문적으로 제한된 버전으로 볼 수 있다. 여기서 제한 사항은 구현자가 더 쉽게 사용할 수 있도록 설계되었다. 사실, OWL 2 Full(RDF 기반 시맨틱에서)은 결정적이지 않기 때문에, OWL 2 DL(Direct Semantics에서)은 원칙적으로 모든 "예 또는 아니오"로 결과를 반환할 수 있는 추론기 작성(자원 제약 조건에 따라 다름)이 가능하다. 설계의 결과로 Direct Semantics에서 전체 OWL 2 DL 언어를 다루는 몇 가지 제품 품질 수준의 추론기가 있다. RDF 기반 시맨틱하의 OWL 2 Full에 대한 이같은 추론기는 없다.
- OWL 2 Full은 RDFS의 가장 직접적인 확장이라고 볼 수 있다. 이와 같이 OWL 2 Full을 위한 RDF 기반 의미 체계는 RDFS 의미 체계와 일반적인 구문 철학을 따른다 (즉, 모든 것이 트리플이고 언어는 완전히 재귀적(fully relfective)이다).

물론 두 의미는 함께 설계되어 서로 영향을 끼쳤다. 예를 들어, OWL 2의 설계 목표 하나는 OWL 2 DL을 OWL 2 Full에 구문적으로 더 가깝게 가져오는 것이었다 (즉, 더 많은 RDF Graphs/OWL 2 Full 온톨로지가 합법적인 OWL 2 DL 온톨로지가 되도록 허용). 이것은 예를 들어 클래스와 개체 모두에 대해 동일한 IRI를 이름으로 사용하는 소위 말장난(punning)을 OWL 2에 통합하게 했다. 그러한 용법의 예는 다음과 같을 것입니다. John은 아버지이고 그 아버지는 사회적 역할이다.

**함수 스타일 syntax**
```
ClassAssertion( :Father :John ) 
ClassAssertion( :SocialRole :Father )
```

첫 번째 명령문에서 `Father`를 클래스로 사용하고 두 번째 명령문에서는 개체로 사용된다. 이런 의미에서 `SocialRole`은 `Father` 클래스의 메타클래스 역할을 한다.

OWL 1에서 이 두 문장을 포함하는 문서는 OWL 1 전체 문서이지만 OWL 1 DL 문서가 아니다. 그러나 OWL 2 DL에서는 이것이 허용된다. 그러나 OWL 2 DL의 직접적 의미는 클래스 `Father`와 `Father` 개체를 동일한 IRI의 두 가지 다른 관점으로 이해함으로써 이를 수용한다는 점에 유의해야 한다. 특히, 두 개체가 동일하면 그들이 나타내는 클래스는 RDF 기반 시맨틱에서도 동등하지만 Direct Semantics에서는 클래스 간에 그러한 관계가 없다. 이것은 아마도 실제로 가장 관련이 있는 두 의미론간의 한 차이점일 것이다.
